//
//  SatScoresViewController.swift
//  NYC Schools
//
//  Created by Wayman Ng on 10/29/18.
//  Copyright © 2018 JPMorgan. All rights reserved.
//

import UIKit

class SatScoresViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var satTakersLabel: UILabel!
    @IBOutlet weak var averageSATReadingLabel: UILabel!
    @IBOutlet weak var averageSATWritingLabel: UILabel!
    @IBOutlet weak var averageSATMathLabel: UILabel!
    
    var highSchool: HighSchool? {
        didSet {
            configureView()
        }
    }
    
    var satScores: SATScores? {
        didSet {
            configureView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
    }
    
    func configureView() {
        guard isViewLoaded else { return }
        
        nameLabel.text = highSchool?.name
        locationLabel.text = highSchool?.location
        overviewLabel.text = highSchool?.overview
        
        satTakersLabel.text = satScores?.numberOfSATTakers ?? "NA"
        averageSATMathLabel.text = satScores?.averageMathScore ?? "NA"
        averageSATWritingLabel.text = satScores?.averageWritingScore ?? "NA"
        averageSATReadingLabel.text = satScores?.averageReadingScore ?? "NA"
    }
}

