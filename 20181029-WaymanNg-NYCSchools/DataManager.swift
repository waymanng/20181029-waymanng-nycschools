//
//  DataManager.swift
//  NYC Schools
//
//  Created by Wayman Ng on 10/29/18.
//  Copyright © 2018 JPMorgan. All rights reserved.
//

import UIKit

// Endpoint info
let domain = "data.cityofnewyork.us"
let token = "vptcGCudm3xUUIxTQZbOVatYS"
let highSchoolsDataSet = "97mf-9njv"
let satScoresDataSet = "734v-jeq5"

/// High School information.
struct HighSchool {
    var id: String
    var name: String
    var location: String?
    var overview: String?
}

/// SAT scores for a specific high school ID.
/// NOTE: Leaving scores data as Strings until a numeric type is needed.
struct SATScores {
    var id: String
    var numberOfSATTakers: String?
    var averageReadingScore: String?
    var averageMathScore: String?
    var averageWritingScore: String?
}

class DataManager {
    
    // Setting up as a singleton for easy access
    static let shared = DataManager()
    
    // Using the third-party SODAKit SDK
    private let sodaClient = SODAClient(domain: domain, token: token)
    
    // Data seems to be static and fairly small, so keeping it around instead of refetching
    private var highSchools: [HighSchool]?
    private var highSchoolSATScores: [SATScores]?
    
    /// Fetch list of high schools, sorted by school name.
    /// Array of high schools returned in completion closure.
    /// Returned error is an NSError type.
    func fetchHighSchools(completion: @escaping (_ schools: [HighSchool]?, _ error: Error?) -> Void) {
        if highSchools != nil {
            // Data already fetched and ready to use
            completion(highSchools, nil)
            
        } else {
            
            let highSchoolsQuery = sodaClient.query(dataset: highSchoolsDataSet)
            
            // Using the status bar's network activiy indicator.
            // Controllers using this method may have to use a better activity indicator.
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            highSchoolsQuery.orderAscending("school_name").get { results in
                switch results {
                case .dataset (let data):
                    self.saveHighSchools(data)
                    completion(self.highSchools, nil)
                    
                case .error (let error):
                    completion(nil, error)
                }
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
    }
    
    /// Fetch SAT scores.
    /// Array of scores returned in completion closure.
    /// Returned error is an NSError type.
    func fetchSATScores(completion: @escaping (_ scores: [SATScores]?, _ error: Error?) -> Void) {
        if highSchoolSATScores != nil {
            // Data already fetched and ready to use
            completion(highSchoolSATScores, nil)
            
        } else {
            
            let satScoresQuery = sodaClient.query(dataset: satScoresDataSet)
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            satScoresQuery.get { results in
                switch results {
                case .dataset (let data):
                    self.saveSATScores(data)
                    completion(self.highSchoolSATScores, nil)
                    
                case .error (let error):
                    completion(nil, error)
                }
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
    }
    
    /// Finds the first matching scores for the given high school ID.
    /// Given the relatively small number of records, will do a simple find first matching the ID.
    /// If data set was larger, would probably implement a hash on the ID to quickly locate the scores for the ID.
    func findSATScores(for id: String) -> SATScores? {
        return highSchoolSATScores?.first(where: { $0.id == id })
    }
    
    /// Converts high school data from the feed to the internal HighSchool structure.
    /// This isolates the specific knowledge of the endpoint to this class to make it easier to change endpoints if necessary.
    private func saveHighSchools(_ data: [[String : Any]]) {
        var newSchools = [HighSchool]()
        
        for school in data {
            // Check for mandatory fields
            guard let id = school["dbn"] as? String,
                let name = school["school_name"] as? String else {
                    continue
            }
            
            let location = school["location"] as? String
            let overview = school["overview_paragraph"] as? String
            
            let highSchool = HighSchool(id: id,
                                        name: name,
                                        location: location,
                                        overview: overview)
            
            newSchools.append(highSchool)
        }
        
        highSchools = newSchools
    }
    
    /// Converts SAT scores data from the feed to the internal SATScores structure.
    /// This isolates the specific knowledge of the endpoint to this class to make it easier to change endpoints if necessary.
    private func saveSATScores(_ data: [[String : Any]]) {
        var newSATScores = [SATScores]()
        
        for scores in data {
            // Check for mandatory fields
            guard let id = scores["dbn"] as? String else {
                continue
            }
            
            let numberOfSATTakers = scores["num_of_sat_test_takers"] as? String
            let averageReadingScore = scores["sat_critical_reading_avg_score"] as? String
            let averageMathScore = scores["sat_math_avg_score"] as? String
            let averageWritingScore = scores["sat_writing_avg_score"] as? String

            let satScores = SATScores(id: id,
                                      numberOfSATTakers: numberOfSATTakers,
                                      averageReadingScore: averageReadingScore,
                                      averageMathScore: averageMathScore,
                                      averageWritingScore: averageWritingScore)
            
            newSATScores.append(satScores)
        }
        
        highSchoolSATScores = newSATScores
    }
}
