//
//  SchoolsTableViewController.swift
//  20181029-WaymanNg-NYCSchools
//
//  Created by Wayman Ng on 10/30/18.
//  Copyright © 2018 JPMorgan. All rights reserved.
//

import UIKit

class SchoolsTableViewController: UITableViewController {
    var highSchools = [HighSchool]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchData()
    }
    
    private func fetchData() {
        DataManager.shared.fetchHighSchools { (highSchools, error) in
            guard let highSchools = highSchools, error == nil else {
                self.showFetchError(error)
                return
            }
            
            self.highSchools = highSchools
        }
        
        // We'll fetch the SAT scores here too so it'll be ready for use.
        DataManager.shared.fetchSATScores { (satScoresArray, error) in
            if let error = error {
                // This error handling might be confusing. Will need to address the user experience when no scores are fetched.
                self.showFetchError(error)
            }
        }
    }
    
    private func showFetchError(_ error: Error?) {
        let errorMessage = (error as NSError?)?.localizedDescription ?? "Unknown Error"
        let alertController = UIAlertController(title: "Error Fetching Data", message: errorMessage, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Retry", style: .default, handler: { (_) in
            self.fetchData()
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let satScoresViewController = segue.destination as? SatScoresViewController,
            let indexPath = tableView.indexPathForSelectedRow {
            
            // Pass along the selected data
            let school = highSchools[indexPath.row]
            satScoresViewController.highSchool = school
            satScoresViewController.satScores = DataManager.shared.findSATScores(for: school.id)
        }
    }
    
    // MARK: - Table View
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return highSchools.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolCell", for: indexPath)
        
        let school = highSchools[indexPath.row]
        
        cell.textLabel?.text = school.name
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "SATScores", sender: self)
    }
}
