//
//  _0181029_WaymanNg_NYCSchoolsTests.swift
//  20181029-WaymanNg-NYCSchoolsTests
//
//  Created by Wayman Ng on 10/30/18.
//  Copyright © 2018 JPMorgan. All rights reserved.
//

import XCTest
@testable import _0181029_WaymanNg_NYCSchools

class _0181029_WaymanNg_NYCSchoolsTests: XCTestCase {
    let testClient = SODAClient (domain: domain, token: token)

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    
    // Test to make sure the max number of records returned by the SDK is not reached.
    // If it has, might need to make subsequent fetches to get all records.
    func testMaxRecordsFetched() {
        let e = expectation(description: "get")
        
        testClient.query(dataset: highSchoolsDataSet).get { result in
            switch result {
            case .dataset (let data):
                XCTAssertLessThan(data.count, 1000, "Size of dataset might be more than max fetched.")
            case .error (let err):
                XCTAssert(false, (err as NSError).userInfo.debugDescription)
            }
            e.fulfill()
        }
        
        waitForExpectations(timeout: 10, handler: nil)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
